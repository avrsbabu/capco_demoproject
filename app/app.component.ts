﻿import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { PagerService } from './services/index'

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    constructor(private http: Http, private pagerService: PagerService) { }

    private allItems: any[];
    pager: any = {};
    pagedItems: any[];

    ngOnInit() {
        this.http.get('./sample_data.json')
            .map((response: Response) => response.json())
            .subscribe(data => {
                this.allItems = data;
                console.log(this.allItems.length);
                this.setPage(1);
            });
    }

    setPage(page: number) {
        this.pager = this.pagerService.getPager(this.allItems.length, page);
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}